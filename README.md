# intranet-website

[![Dependency Status](https://david-dm.org/closingin/intranet-website.svg)](https://david-dm.org/closingin/intranet-website)
[![devDependencies Status](https://david-dm.org/closingin/intranet-website/dev-status.svg)](https://david-dm.org/closingin/intranet-website?type=dev)
[![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://github.com/closingin/intranet-website/issues)
[![GitHub license](https://img.shields.io/badge/license-GPLv3-blue.svg)](https://github.com/closingin/intranet-website/blob/master/LICENSE)
[![GitHub version](https://img.shields.io/github/release/closingin/intranet-website.svg?label=version)](https://github.com/closingin/intranet-website/releases/latest)

## Getting started

You need to download and start the brand new API that goes with this project at
[closingin/intranet-api](https://github.com/closingin/intranet-api) before you
can try this one out. The reference to this API is in `src/main.js`. Keep in
mind that this project is a work in progress.

## Installation

From the command line :

```bash
npm i
```

## Running the server

From the command line :

```bash
npm start
```
