import * as NG from './guards'

import UsersProfile from '@/components/pages/Users/Profile'
import UsersResults from '@/components/pages/Users/Results'
import UsersInternships from '@/components/pages/Users/Internships'
import UsersDocuments from '@/components/pages/Users/Documents'

export default [
    {
        path: '/users/:email',
        name: 'user',
        beforeEnter: NG.IsAuthenticated,
        component: UsersProfile
    }, {
        path: '/users/:email/results',
        name: 'user.results',
        beforeEnter: NG.IsAuthenticated,
        component: UsersResults
    }, {
        path: '/users/:email/internships',
        name: 'user.internships',
        beforeEnter: NG.IsAuthenticated,
        component: UsersInternships
    }, {
        path: '/users/:email/documents',
        name: 'user.documents',
        beforeEnter: NG.IsAuthenticated,
        component: UsersDocuments
    }
]
