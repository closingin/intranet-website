import * as NG from './guards'

import SchoolingSchedule from '@/components/pages/Schooling/Schedule'
import SchoolingUnits from '@/components/pages/Schooling/Units'
import SchoolingUnitsEntry from '@/components/pages/Schooling/UnitsEntry'
import SchoolingCourses from '@/components/pages/Schooling/Courses'

export default [
    {
        path: '/schedule',
        name: 'schedule',
        beforeEnter: NG.IsAuthenticated,
        component: SchoolingSchedule
    }, {
        path: '/units',
        name: 'units',
        beforeEnter: NG.IsAuthenticated,
        component: SchoolingUnits
    }, {
        path: '/units/:id',
        name: 'units.entry',
        beforeEnter: NG.IsAuthenticated,
        component: SchoolingUnitsEntry
    }, {
        path: '/courses',
        name: 'courses',
        beforeEnter: NG.IsAuthenticated,
        component: SchoolingCourses
    }
]
