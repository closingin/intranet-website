import { TokenExists } from '@/assets/utilities/authentication'

export function IsAuthenticated (to, from, next) {
    if (!TokenExists()) {
        next({
            name: 'account.login',
            query: {
                redirect: to.fullPath
            }
        })
    } else next()
}

export function IsNotAuthenticated (to, from, next) {
    if (TokenExists()) {
        next({
            name: 'home'
        })
    } else next()
}
