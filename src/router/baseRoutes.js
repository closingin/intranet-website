import * as NG from './guards'

import Home from '@/components/pages/Home'

export default [
    {
        path: '/',
        name: 'home',
        beforeEnter: NG.IsAuthenticated,
        component: Home
    }
]
