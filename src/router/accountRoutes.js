import * as NG from './guards'

import AccountLogin from '@/components/pages/Account/Login'
import AccountLogout from '@/components/pages/Account/Logout'
import AccountSettings from '@/components/pages/Account/Settings'

export default [
    {
        path: '/account/login',
        name: 'account.login',
        beforeEnter: NG.IsNotAuthenticated,
        component: AccountLogin
    }, {
        path: '/account/logout',
        name: 'account.logout',
        beforeEnter: NG.IsAuthenticated,
        component: AccountLogout
    }, {
        path: '/account/settings',
        name: 'account.settings',
        beforeEnter: NG.IsAuthenticated,
        component: AccountSettings
    }
]
