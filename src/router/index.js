import Vue from 'vue'
import Router from 'vue-router'

import baseRoutes from './baseRoutes'
import accountRoutes from './accountRoutes'
import schoolingRoutes from './schoolingRoutes'
import usersRoutes from './usersRoutes'
import systemRoutes from './systemRoutes'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: Array.concat(
        baseRoutes,
        accountRoutes,
        schoolingRoutes,
        usersRoutes,
        systemRoutes
    )
})
