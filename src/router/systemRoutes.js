import System404 from '@/components/pages/System/404'

export default [
    {
        path: '*',
        name: 'system.404',
        component: System404
    }
]
