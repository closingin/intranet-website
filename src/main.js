import Vue from 'vue'
import Axios from 'axios'
import VueAxios from 'vue-axios'

import router from './router'

import App from './App'

Vue.use(VueAxios, Axios.create({
    baseURL: 'http://127.0.0.1:4200/'
}))

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    template: '<App />',
    components: { App }
})
