import jwt from 'jsonwebtoken'

export function SetToken (token) {
    localStorage.setItem('token', token)
}

export function GetToken () {
    return localStorage.getItem('token')
}

export function GetTokenPayload () {
    return jwt.decode(GetToken())
}

export function DeleteToken () {
    localStorage.removeItem('token')
}

export function TokenExists () {
    return !!GetToken()
}
