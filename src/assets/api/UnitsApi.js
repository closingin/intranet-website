import Vue from 'vue'

import { GetOptions, BeautifyError } from './utilities'

export default class UnitsApi {
    static GetCurrentUnits (email) {
        return Vue.axios.get('/units/current', GetOptions())
            .then((resp) => resp.data)
            .catch((err) => {
                throw BeautifyError(err)
            })
    }

    static GetUnit (id) {
        return Vue.axios.get(`/units/${id}`, GetOptions())
            .then((resp) => resp.data)
            .catch((err) => {
                throw BeautifyError(err)
            })
    }

    static Register (id) {
        return Vue.axios.get(`/units/${id}/register`, GetOptions())
            .catch((err) => {
                throw BeautifyError(err)
            })
    }

    static Unregister (id) {
        return Vue.axios.get(`/units/${id}/unregister`, GetOptions())
            .catch((err) => {
                throw BeautifyError(err)
            })
    }
}
