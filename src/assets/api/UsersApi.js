import Vue from 'vue'

import { GetOptions, BeautifyError } from './utilities'
import { GetTokenPayload } from '@/assets/utilities/authentication'

export default class UserApi {
    static GetProfile (email) {
        return Vue.axios.get(`/users/${email}`, GetOptions())
            .then((resp) => resp.data)
            .catch((err) => {
                throw BeautifyError(err)
            })
    }

    static GetNotifications (type) {
        return Vue.axios.get(`/users/${GetTokenPayload().email}/notifications/${type}`, GetOptions())
            .then((resp) => resp.data)
            .catch((err) => {
                throw BeautifyError(err)
            })
    }
}
