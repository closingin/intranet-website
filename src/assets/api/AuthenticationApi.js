import Vue from 'vue'

import { GetOptions, BeautifyError } from './utilities'
import { SetToken, DeleteToken } from '@/assets/utilities/authentication'

export default class AuthenticationApi {
    static CookieLogin (cookie) {
        return AuthenticationApi.Login({ cookie })
    }

    static CredentialsLogin (username, password) {
        return AuthenticationApi.Login({ username, password })
    }

    static Login (data) {
        return Vue.axios.post('/login', data, GetOptions())
            .then((resp) => {
                SetToken(resp.data.token)
            })
            .catch((err) => {
                throw BeautifyError(err)
            })
    }

    static Logout () {
        return Vue.axios.delete('/logout', GetOptions())
            .then(() => {
                DeleteToken()
            })
            .catch((err) => {
                DeleteToken()
                throw BeautifyError(err)
            })
    }
}
