import { GetToken } from '@/assets/utilities/authentication'

export function GetHeaders () {
    let headers = {}
    const token = GetToken()

    if (token) {
        headers.Authorization = `Bearer ${token}`
    }

    return headers
}

export function GetOptions () {
    return {
        headers: GetHeaders()
    }
}

export function BeautifyError (err) {
    switch (err.response.data.message) {
    case 'INTRANET_DOWN':
        return 'Oops, it looks like the official intranet is currently down!'
    case 'INVALID_CREDS':
    case 'INVALID_COOKIE':
        return 'Wrong credentials, dude.'
    case 'USER_NOT_REGISTERED_TO_UNIT':
        return 'You are not registered to this unit.'
    case 'USER_ALREADY_REGISTERED_TO_UNIT':
        return 'You are already registered to this unit.'
    case 'UNIT_NOT_AVAILABLE':
        return 'This unit is not yet available, try again later.'
    default:
        return 'Something happened (lel)'
    }
}
