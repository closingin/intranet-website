
export default class Notifications {
    static notify (message, type) {
        Notifications._createContainer()
        Notifications._createNotification(message, type)
    }

    static _createContainer () {
        if (document.getElementsByClassName('notifications')[0]) {
            return
        }

        let container = document.createElement('div')
        container.classList.add('notifications')
        document.body.appendChild(container)
    }

    static _createNotification (message, type) {
        let container = document.getElementsByClassName('notifications')[0]
        let notification = document.createElement('div')
        let icon = 'info-circle'

        if (type === 'success') {
            icon = 'check-circle'
        } else if (type === 'error') {
            icon = 'exclamation-triangle'
        }

        notification.classList.add('notification')
        notification.classList.add(type)
        notification.innerHTML = `
            <div class="icon"><i class="fa fa-${icon}"></i></div>
            <div class="text"><p>${message}</p></div>
        `

        container.insertBefore(notification, container.firstChild)

        setTimeout(() => {
            notification.classList.add('active')
        }, 10)

        let timeout = setTimeout(() => {
            Notifications._deleteNotification(container, notification)
        }, 4000)

        notification.addEventListener('click', (e) => {
            clearTimeout(timeout)
            Notifications._deleteNotification(container, notification)
        })
    }

    static _deleteNotification (container, notification) {
        notification.classList.remove('active')
        setTimeout(() => {
            container.remove(notification)
            if (!container.hasChildNodes()) {
                document.body.removeChild(container)
            }
        }, 300)
    }
}
